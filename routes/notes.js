var express = require('express');
var NoteController = require("../controllers/note");
var router = express.Router();

router.get('/', NoteController.displayAllNotes);

router.post('/create', NoteController.createNote);

router.get('/:id', NoteController.displayNote);

router.post('/:id', NoteController.updateNote);

router.delete('/:id', NoteController.deleteNote);

module.exports = router;
