var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var NoteSchema = new Schema(
		{
			title       : {type : String, required : true, max : 100},
			body        : {type : String, required : true, max : 500},
			priority    : Number,
			pinnedToTop : Boolean
		}	
);

module.exports = mongoose.model("Note", NoteSchema);
