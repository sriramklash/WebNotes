var Note = require("../models/note");
var async = require('async');

exports.createNoteGet = function(req, res) {
	res.render('new_note');
}

exports.createNotePost = function(req, res) {
	var note = new Note({
		title : req.body.title,
		body  : req.body.body
	});

	note.save(function(err) {
		if (err) {
			console.error(err);
			return err;
		}

		res.redirect('/notes');
		// window.alert("Note saved!");
	});
}

exports.displayAllNotes = function(req, res) {
	Note.find({}, function(err, notesList) {
		if (err) {
			console.error(err);
			return err;
		}

		res.render('notes', {notes : notesList});
	});
}

exports.displayNote = function(req, res) {
	Note.findById(req.params.id, function(err, note) {
		if (err) return err;

		res.render('display_note', {note : note});
	});
}

exports.deleteNote = function(req, res) {
	Note.remove({id : req.params.id}, function(err, callback) {
		if (err) return err;
		console.log("Note with id " + req.params.id + "removed.");
		res.redirect('/notes/');
	});
}

exports.updateNote = function(req, res) {

}
